# %% 
import requests
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta
import csv

# %%
IDs = {'Hilda':3118,'Maligne':3120}
URL = ['https://secure.webrez.com/Bookings105/activity-edit.html?table=hotels&listing_id=','&mode=ajax&command=roomsearch_version2_invtype_result&hotel_id=3118&merchant_id=3118&date_from=','&date_to=,','&num_children=0&num_babies=0&reservationcode_id=24392&sourcecode_id=&num_adults_male=1&num_adults_female=0&flag_createfolio=0&duplicate_id=0&command=duplicate&flag_perl_bookingengine=1&num_adults=1&duplicate_includeiata=0&num_beds=0&access_code=&transaction_id=-1&folio_id=0&customer_id=0&reservation_password=&flag_changemode=0&flag_nophotos=1&invtype_id=44237&corpaccount_id=&language=english']

# %%
# Functions
def generate_date_range():
    today = datetime.now().date()
    one_year_from_today = today + timedelta(days=365)
    date_range = [today + timedelta(days=x) for x in range((one_year_from_today - today).days + 1)]
    return date_range

def download_urls_from_csv(startDate,hostelID,URL):
    endDate = (startDate+timedelta(days=1)).strftime('%Y%m%d')
    startDate
    URL = URL[0]+str(hostelID)+URL[1]+startDate.strftime('%Y%m%d')+URL[2]+endDate+URL[3]

    # Download the content of the URL
    response = requests.get(URL)
    
    if response.status_code == 200 and response.content:
        # Save the content to a file in the output folder
        # with open(f"{output_folder}/{filename}", 'wb') as file:
            # file.write(response.content)
        return startDate
    # else:
        # print(f"Failed to download {startDate}. Status code: {response.status_code}")

def get_responses_for_dates_parallel(date_list,hostelID=IDs['Hilda'],URL=URL):
    responses = []

    # Set the maximum number of concurrent requests (adjust as needed)
    max_workers = min(100, len(date_list))

    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        # Use executor.map to apply fetch_url to each date in parallel
        results = executor.map(lambda date: download_urls_from_csv(date,hostelID,URL), date_list)

    # Collect the results
    for result in results:
        responses.append(result)

    return responses
# %%
if __name__ == "__main__":
    date_range = generate_date_range()
    validDates = get_responses_for_dates_parallel(date_range)
    validDates = [i for i in validDates if i is not None]
    
    with open('validDates.csv', 'w', newline='') as file:
        wr = csv.writer(file,delimiter='\n')
        wr.writerow(validDates)
