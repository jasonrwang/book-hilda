# Booking Tricky Wilderness Huts

HI's Hilda Hut, and a few others, annoyingly go to a webrez website that doesn't easily show room availability.

This script looks through huts – beginning with the Hilda Ridge hut – to just spit out available dates.
